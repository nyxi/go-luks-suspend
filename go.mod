module gitlab.com/nyxi/go-luks-suspend

go 1.21.3

require github.com/guns/golibs v0.16.0

require golang.org/x/sys v0.13.0 // indirect
